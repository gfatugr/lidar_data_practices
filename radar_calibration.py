#!/usr/bin/env python
# coding: utf-8

import os
import gzip
import shutil
import tempfile
import math
import glob
import datetime as dt
import xarray as xr
import numpy as np
from scipy import stats
from scipy.signal import correlate
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib
from netCDF4 import Dataset


def open_netcdf(fname):
    if fname.endswith(".gz"):
        infile = gzip.open(fname, 'rb')
        tmp = tempfile.NamedTemporaryFile(delete=False)
        shutil.copyfileobj(infile, tmp)
        infile.close()
        tmp.close()
        #data = xr.open_dataset(tmp.name)
        data = Dataset(tmp.name)
        os.unlink(tmp.name)
    else:
        #data = xr.open_dataset(fname)
        data = Dataset(fname)
    return data 

class BinnedPSD():
    """Binned gamma particle size distribution (PSD).

    Callable class to provide a binned PSD with the given bin edges and PSD
    values.

    Args (constructor):
        The first argument to the constructor should specify n+1 bin edges,
        and the second should specify n bin_psd values.

    Args (call):
        D: the particle diameter.

    Returns (call):
        The PSD value for the given diameter.
        Returns 0 for all diameters outside the bins.
    """

    def __init__(self, bin_edges, bin_psd):
        if len(bin_edges) != len(bin_psd)+1:
            raise ValueError("There must be n+1 bin edges for n bins.")

        self.bin_edges = bin_edges
        self.bin_psd = bin_psd

    def psd_for_D(self, D):
        if not (self.bin_edges[0] < D <= self.bin_edges[-1]):
            return 0.0

        # binary search for the right bin
        start = 0
        end = len(self.bin_edges)
        while end-start > 1:
            half = (start+end)//2
            if self.bin_edges[start] < D <= self.bin_edges[half]:
                end = half
            else:
                start = half

        return self.bin_psd[start]

    def __call__(self, D):
        if np.shape(D) == (): # D is a scalar
            return self.psd_for_D(D)
        else:
            return np.array([self.psd_for_D(d) for d in D])

    def __eq__(self, other):
        if other is None:
            return False
        return len(self.bin_edges) == len(other.bin_edges) and (self.bin_edges == other.bin_edges).all() and  (self.bin_psd == other.bin_psd).all() 

def getEdges():

    pars_class = np.zeros(shape=(32,2))
    bin_edges = np.zeros(shape=(33,1))

    #pars_class[:,0] : Center of Class [mm]
    #pars_class[:,1] : Width of Class [mm]
    pars_class[0:10,1] = 0.125
    pars_class[10:15,1] = 0.250
    pars_class[15:20,1] = 0.500
    pars_class[20:25,1] = 1.
    pars_class[25:30,1] = 2.
    pars_class[30:32,1] = 3.

    j = 0
    pars_class[0,0] = 0.062
    for i in range(1,32):
        if i < 10 or (i > 10 and i < 15) or (i > 15 and i < 20) or (i > 20 and i < 25) or (i > 25 and i < 30) or (i > 30):
            pars_class[i,0] = pars_class[i-1,0] + pars_class[i,1]

        const = [0.188, 0.375, 0.75, 1.5, 2.5]
        if i == 10 or i == 15 or i == 20 or i == 25 or i == 30:
            pars_class[i,0] = pars_class[i-1,0] + const[j]
            j = j + 1

    #print pars_class[i,0]
        bin_edges[i+1,0] = pars_class[i,0] + pars_class[i,1]/2


    bin_edges[0,0] = 0.
    bin_edges[1,0] = pars_class[0,0] + pars_class[0,1]/2 
    
    return bin_edges

def loadwbandgra(wbandgraFileList):
    wbandgraZe = xr.Dataset()

    #load the files
    for filePath in wbandgraFileList:

        tempDSZe = xr.open_dataset(filePath)

        wbandgraZe = xr.merge([wbandgraZe,
                              tempDSZe])

        tempDSZe.close()
    return wbandgraZe

def histogram_intersection(h1, h2, bins):
    bins = np.diff(bins)
    sm = 0
    for i in range(len(bins)):
        sm += min(bins[i]*h1[i], bins[i]*h2[i])
    return sm

def pars2reflec(filepath_rainscat, parsFilePath, freq, surf_temp, startDate, stopDate): #format='%Y-%m-%d %H:%M'
    # # Parsivel Ze
    freqstr = '%3.1f' % freq
    if (surf_temp < 5):
        scattabstr = '0.C_%sGHz.csv' % freqstr
    elif  (5 <= surf_temp < 15):
        scattabstr = '10C_%sGHz.csv' % freqstr
    else: 
        scattabstr = '20C_%sGHz.csv' % freqstr
        
    filename = os.path.join(filepath_rainscat, scattabstr)     
    print('filename')
    print(filename)
    if os.path.isfile(filename):
        #reading scattering table
        df = pd.read_csv(filename) 
    else:
        print('Error! %s does not exists' % filename)
        return
    diameter = 'diameter[mm]'
    radarxs = 'radarsx[mm2]'
    wavelength = 'wavelength[mm]'
    temp = 'T[k]'
    extxs = 'extxs[mm2]'

    delta = 0.01
    upscale_end = (len(df)+1.)/100.
    diameter_ups = np.arange(delta,upscale_end,delta)

    #constants
    T = df.loc[1,temp]
    wavelen = df.loc[1,wavelength]
    K2 = 0.93

    #integration constant
    int_const = wavelen**4 / ((math.pi)**5 *K2)

    #reding meang volume diameter from parsivel
    print('parsFilePath')
    print(parsFilePath)
    pasrDS = xr.open_dataset(parsFilePath)    
       
    #select time range    
    before=( pd.to_datetime(startDate,format='%Y-%m-%d %H:%M') < pd.to_datetime(pasrDS["time"].data))
    after = ( pd.to_datetime(stopDate,format='%Y-%m-%d %H:%M') > pd.to_datetime(pasrDS["time"].data))
    mask_time = before & after
                          
    if not any(mask_time):
        print('No rain in this user-provided period.')
        return
    else:
        parstime = pasrDS["time"].data[mask_time]
        N = pasrDS['N'][:,mask_time]
        N = 10**N.T
        #print(N)
        
    #calculating Ze using parsivel SD    
    Zpars = np.zeros(N.shape[0])    
    if freq > 20:        
        #Ze parsivel using T-matrix at "freq" GHz
        for i in range(N.shape[0]):
            PSD = BinnedPSD(getEdges(),N[i])                                        
            y = PSD(diameter_ups)*df.loc[:,radarxs]
            Zpars[i] = int_const * y.sum()*delta
    else:
        #Ze parsivel in Rayleigh regime
        for i in range(N.shape[0]):
            PSD = BinnedPSD(getEdges(),N[i])
            d6 = PSD(diameter_ups)*df.loc[:,diameter]**6
            Zpars[i] = d6.sum()*delta 
      
    parsZe = 10 * np.log10(Zpars)
    pars = xr.Dataset({'parsZe': (['time'],  parsZe)}, coords={'time': parstime})
    return pars

def quicklook_radar(figpath, radar, variable, year, month, day, hourstart, minstart):
    matplotlib.rcParams.update({'font.size': 22})
    Vmin = {'Zg': -40, 'corrected_reflectivity': -40, 'VELg': -4}
    Vmax = {'Zg': 20, 'corrected_reflectivity': 20, 'VELg': 4}
    
    plt.figure(figsize=(18,5))
    radar[variable].T.plot(cmap='jet', vmax=Vmax[variable], vmin=Vmin[variable])
    plt.grid()
    figstr = 'quicklok_%s_%s%s%s_%s%s_.png' % (variable, year, month, day, hourstart, minstart)
    plt.savefig(os.path.join(figpath, figstr), dpi=400)